# BentoQuizz

Un jeu de quizz avec ses buzzers.

## Mécanique
### Anti spam
Si un joueur spam le buzzer plus de X fois en X secondes, il recoit une pénalité de X secondes durant lesquelles il ne peut plus buzzer.
Cela pourrait se traduire visuellement avec une barre fonctionnant à la manière d'une stamina/endurance dans un jeu vidéo. Plus on appuie sur le bouton frénétiquement, plus il monte jusqu'à son seuil. si on arrête son niveau redescend lentement. Si le seuil est atteint, il se bloque.

## Mode 1 réponse
Quand une équipe appuie sur un buzzer, les autres buzzer sont désactivés. Si la réponse est bonne, tous les buzzers sont disponibles, si la réponse est mauvaise, les autres buzzers sont disponibles mais celui de l'équipe ayant appuyé est verrouillé jusqu'à la prochaine question.

